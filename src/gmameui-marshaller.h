
#ifndef __gmameui_marshaller_MARSHAL_H__
#define __gmameui_marshaller_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:STRING,INT,INT (gmameui-marshallers.list:2) */
extern void gmameui_marshaller_VOID__STRING_INT_INT (GClosure     *closure,
                                                     GValue       *return_value,
                                                     guint         n_param_values,
                                                     const GValue *param_values,
                                                     gpointer      invocation_hint,
                                                     gpointer      marshal_data);

/* VOID:STRING (gmameui-marshallers.list:3) */
#define gmameui_marshaller_VOID__STRING	g_cclosure_marshal_VOID__STRING

/* VOID:INT (gmameui-marshallers.list:4) */
#define gmameui_marshaller_VOID__INT	g_cclosure_marshal_VOID__INT

/* VOID:BOOLEAN (gmameui-marshallers.list:5) */
#define gmameui_marshaller_VOID__BOOLEAN	g_cclosure_marshal_VOID__BOOLEAN

/* VOID:POINTER (gmameui-marshallers.list:6) */
#define gmameui_marshaller_VOID__POINTER	g_cclosure_marshal_VOID__POINTER

G_END_DECLS

#endif /* __gmameui_marshaller_MARSHAL_H__ */

