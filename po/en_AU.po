# GMAMEUI is a GTK+ MAME front-end
# Copyright (C) 2007-2008 Andrew Burton
# This file is distributed under the same license as the gmameui package.
# Andrew Burton <adb@iinet.net.au>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: GMAMEUI 0.2.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-06-07 17:11+1000\n"
"PO-Revision-Date: 2008-06-05 17:34+1000\n"
"Last-Translator: Andrew Burton <adb@iinet.net.au>\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/about.c:51
msgid "translator-credits"
msgstr "Andrew Burton"

#: ../src/about.c:59 ../src/about.c:236
msgid "Copyright (c) 2007-2008 Andrew Burton"
msgstr ""

#: ../src/about.c:61 ../src/about.c:232
msgid "A program to play MAME under Linux"
msgstr ""

#: ../src/about.c:106 ../src/about.c:206
msgid "Credits"
msgstr ""

#: ../src/about.c:122
msgid "Developers"
msgstr ""

#: ../src/about.c:123
msgid ""
"Andrew Burton\n"
"Code based on GXMame\n"
"Stephane Pontier <shadow_walker@users.sourceforge.net>\n"
"Benoit Dumont <vertigo17@users.sourceforge.net>\n"
"Nicos Panayides <anarxia@gmx.net>\n"
"Priit Laes <x-laes@users.sourceforge.net>\n"
"William Jon McCann <mccann@jhu.edu>\n"
msgstr ""

#: ../src/about.c:134
msgid "Translators"
msgstr ""

#: ../src/about.c:135
msgid ""
"The following people translated GMAMEUI:\n"
"Brazilian Portugese (pt_BR) - Alfredo Jr <junix@linuxinabox.org>\n"
"Italian (it) - Ugo Viti <ugo.viti@initzero.it>\n"
msgstr ""

#: ../src/about.c:202
msgid "About GMAMEUI"
msgstr ""

#: ../src/about.c:224
msgid "GMAMEUI"
msgstr ""

#: ../src/audit.c:252
msgid "Don't know how to verify roms with this version of xmame."
msgstr ""

#: ../src/audit.c:346 ../src/audit.c:414
msgid "Audit Stopped"
msgstr ""

#: ../src/audit.c:347 ../src/audit.c:415
msgid "Stopped"
msgstr ""

#: ../src/audit.c:419 ../src/audit.c:422
msgid "Audit done"
msgstr ""

#: ../src/audit.c:420
msgid "Done"
msgstr ""

#: ../src/callbacks.c:241 ../src/gmameui.c:642
msgid "No xmame executables defined"
msgstr ""

#: ../src/callbacks.c:776
#, c-format
msgid "Hide \"%s\" Column"
msgstr ""

#: ../src/directories.c:114
msgid "Browse for Folder"
msgstr ""

#: ../src/directories.c:121
msgid "Browse for File"
msgstr ""

#: ../src/directories.c:760
msgid "No valid xmame executables found"
msgstr ""

#: ../src/game_list.c:203 ../src/io.c:210 ../src/io.c:213
#: ../src/properties.c:119
msgid "Unknown"
msgstr ""

#: ../src/game_list.c:239
msgid "Loading gamelist"
msgstr ""

#: ../src/game_list.c:274
msgid "Game list is corrupted."
msgstr ""

#: ../src/game_list.c:286
msgid "Out of memory while loading gamelist"
msgstr ""

#: ../src/game_list.c:415
#, c-format
msgid "Loaded %d roms by %d manufacturers covering %d years."
msgstr ""

#: ../src/game_list.c:417
#, c-format
msgid "with %d games supporting samples."
msgstr ""

#: ../src/game_list.c:577
msgid "Saving gamelist."
msgstr ""

#: ../src/game_list.c:628
msgid ""
"Could not recognise the gamelist version.\n"
"Do you want to rebuild the gamelist?"
msgstr ""

#: ../src/game_list.c:637
msgid ""
"Gamelist not available,\n"
"Do you want to build the gamelist?"
msgstr ""

#: ../src/game_list.c:646
msgid ""
"Gamelist was created with an older version of GMAMEUI.\n"
"The gamelist is not supported.\n"
"Do you want to rebuild the gamelist?"
msgstr ""

#: ../src/game_list.c:659
#, c-format
msgid ""
"The gamelist is from:\n"
"%s %s\n"
"and the current executable is:\n"
"%s %s\n"
"Do you want to rebuild the gamelist?"
msgstr ""

#: ../src/gui.c:767
#, c-format
msgid "Time to initialise icons: %.02f seconds"
msgstr ""

#: ../src/gui.c:772
#, c-format
msgid "Time to create main window and filters: %.02f seconds"
msgstr ""

#: ../src/gui.c:829
#, c-format
msgid "Time to create gamelist: %.02f seconds"
msgstr ""

#: ../src/gui.c:834
#, c-format
msgid "Time to create gamelist content: %.02f seconds"
msgstr ""

#: ../src/gui.c:860
#, c-format
msgid "Time to complete start of UI: %.02f seconds"
msgstr ""

#: ../src/gui.c:881
#, c-format
msgid "%s is not a valid executable"
msgstr ""

#: ../src/gui.c:1605
msgid "Wait..."
msgstr ""

#: ../src/gui.c:1666 ../src/gui.c:2053
msgid "Yes"
msgstr ""

#: ../src/gui.c:1666 ../src/gui.c:2053
msgid "No"
msgstr ""

#. Status Bar Message
#: ../src/gui.c:1774
msgid "games"
msgstr ""

#: ../src/gui.c:1774
msgid "game"
msgstr ""

#. update statusbar
#: ../src/gui.c:1968
msgid "No game selected"
msgstr ""

#: ../src/gui.c:2120
#, c-format
msgid ""
"A file named '%s' already exists.\n"
"Do you want to overwrite it?"
msgstr ""

#: ../src/gui.c:2148
msgid "Choose inp file to play"
msgstr ""

#: ../src/gui.c:2155
msgid "Choose inp file to record"
msgstr ""

#: ../src/gmameui.c:86
msgid "No xmame executable found"
msgstr ""

#: ../src/gmameui.c:155
msgid "dirs.ini not loaded, using default values"
msgstr ""

#: ../src/gmameui.c:162
#, c-format
msgid "Time to initialise: %.02f seconds"
msgstr ""

#: ../src/gmameui.c:165
msgid "gamelist not found, need to rebuild one"
msgstr ""

#: ../src/gmameui.c:168
#, c-format
msgid "Time to load gamelist: %.02f seconds"
msgstr ""

#: ../src/gmameui.c:171
msgid "games.ini not loaded, using default values"
msgstr ""

#: ../src/gmameui.c:173
#, c-format
msgid "Time to load games ini: %.02f seconds"
msgstr ""

#: ../src/gmameui.c:176
msgid "catver not loaded, using default values"
msgstr ""

#: ../src/gmameui.c:182
msgid "unable to load gmameui.ini, using default values"
msgstr ""

#: ../src/gmameui.c:185
msgid "default options not loaded, using default values"
msgstr ""

#: ../src/gmameui.c:189
#, c-format
msgid "Time to initialise GMAMEUI: %.02f seconds"
msgstr ""

#: ../src/gmameui.c:204
#, c-format
msgid "Joystick %s found"
msgstr ""

#: ../src/gmameui.c:206
msgid "No Joystick found"
msgstr ""

#: ../src/gmameui.c:377 ../src/gmameui.c:385
#, c-format
msgid "Loading %s:"
msgstr ""

#. if not print a message and return to the selection screen
#: ../src/gmameui.c:636
#, c-format
msgid "Could not open '%s' as valid input file"
msgstr ""

#: ../src/gmameui.c:710
msgid "Exiting GMAMEUI..."
msgstr ""

#: ../src/gmameui.c:741
msgid "Finished cleaning up GMAMEUI"
msgstr ""

#: ../src/gmameui.c:771
msgid "Game"
msgstr ""

#: ../src/gmameui.c:773 ../data/directories.glade.h:24
msgid "ROMs"
msgstr ""

#: ../src/gmameui.c:775 ../src/interface.c:585 ../data/directories.glade.h:25
msgid "Samples"
msgstr ""

#: ../src/gmameui.c:777 ../data/gmameui_prefs.glade.h:9
msgid "Directory"
msgstr ""

#: ../src/gmameui.c:779 ../src/interface.c:501
msgid "Played"
msgstr ""

#: ../src/gmameui.c:781 ../data/gmameui_prefs.glade.h:15
msgid "Manufacturer"
msgstr ""

#: ../src/gmameui.c:783 ../data/gmameui_prefs.glade.h:20
msgid "Year"
msgstr ""

#: ../src/gmameui.c:785 ../data/gmameui_prefs.glade.h:8
msgid "Clone of"
msgstr ""

#: ../src/gmameui.c:787 ../data/gmameui_prefs.glade.h:10
msgid "Driver"
msgstr ""

#. Available / Not Available
#: ../src/gmameui.c:789
msgid "Status"
msgstr ""

#: ../src/gmameui.c:791
msgid "Rom of"
msgstr ""

#. Working / Not Working
#: ../src/gmameui.c:793
msgid "Driver Status"
msgstr ""

#: ../src/gmameui.c:795
msgid "Driver Colors"
msgstr "Driver Colours"

#: ../src/gmameui.c:797
msgid "Driver Sound"
msgstr ""

#: ../src/gmameui.c:799
msgid "Driver Graphics"
msgstr ""

#: ../src/gmameui.c:801
msgid "Players"
msgstr ""

#: ../src/gmameui.c:803
msgid "Buttons"
msgstr ""

#: ../src/gmameui.c:805 ../data/gmameui_prefs.glade.h:19
msgid "Version"
msgstr ""

#: ../src/gmameui.c:807 ../data/gmameui_prefs.glade.h:6
msgid "Category"
msgstr ""

#: ../src/gmameui.c:809
msgid "Favorite"
msgstr "Favourite"

#: ../src/gmameui.c:811
msgid "Channels"
msgstr ""

#: ../src/gmameui.c:813 ../src/interface.c:473
msgid "BIOS"
msgstr ""

#: ../src/interface.c:112
msgid "GMAMEUI Arcade Machine Emulator"
msgstr ""

#: ../src/interface.c:235
msgid "Play Game"
msgstr ""

#: ../src/interface.c:240
msgid "Show Folders"
msgstr ""

#: ../src/interface.c:251
msgid "Show Sidebar"
msgstr ""

#: ../src/interface.c:256
msgid "List"
msgstr ""

#: ../src/interface.c:264
msgid "List Tree"
msgstr ""

#: ../src/interface.c:272 ../data/properties.glade.h:72
#: ../data/rom_info.glade.h:8
msgid "Details"
msgstr ""

#: ../src/interface.c:280
msgid "Details Tree"
msgstr ""

#: ../src/interface.c:317
msgid "All ROMs"
msgstr ""

#: ../src/interface.c:331
msgid "Available"
msgstr ""

#: ../src/interface.c:349
msgid "Unavailable"
msgstr ""

#: ../src/interface.c:364 ../src/properties.c:117 ../src/properties.c:160
msgid "Incorrect"
msgstr ""

#: ../src/interface.c:382
msgid "Neo-Geo"
msgstr ""

#: ../src/interface.c:395
msgid "CPS1"
msgstr ""

#: ../src/interface.c:408
msgid "CPS2"
msgstr ""

#: ../src/interface.c:421
msgid "CPS3"
msgstr ""

#: ../src/interface.c:434
msgid "Sega System 16"
msgstr ""

#: ../src/interface.c:447
msgid "Sega Model 2"
msgstr ""

#: ../src/interface.c:460
msgid "Namco System 22"
msgstr ""

#: ../src/interface.c:487
msgid "Favorites"
msgstr "Favourites"

#: ../src/interface.c:516
msgid "Colors"
msgstr "Colours"

#: ../src/interface.c:530 ../src/options.c:1755 ../data/properties.glade.h:136
msgid "Sound"
msgstr ""

#: ../src/interface.c:544
msgid "Graphics"
msgstr ""

#: ../src/interface.c:559
msgid "Originals"
msgstr ""

#: ../src/interface.c:572
msgid "Clones"
msgstr ""

#: ../src/interface.c:599
msgid "Stereo"
msgstr ""

#: ../src/interface.c:613
msgid "Raster"
msgstr ""

#: ../src/interface.c:626 ../src/options.c:2613 ../data/options.glade.h:76
#: ../data/properties.glade.h:154
msgid "Vector"
msgstr ""

#: ../src/interface.c:666
msgid "Trackball"
msgstr ""

#: ../src/interface.c:680
msgid "Lightgun"
msgstr ""

#: ../src/io.c:475
msgid "Performing quick check, please wait:"
msgstr ""

#: ../src/keyboard.c:80
msgid "French Layout"
msgstr ""

#: ../src/keyboard.c:81
msgid "German Layout"
msgstr ""

#: ../src/mameio.c:538 ../src/mameio.c:688
msgid "Creating game list..."
msgstr ""

#: ../src/mameio.c:539
msgid "Creating game list, please wait"
msgstr ""

#: ../src/mameio.c:566 ../src/mameio.c:709
#, c-format
msgid "Creating game list (%d games)..."
msgstr ""

#: ../src/mameio.c:608
#, c-format
msgid "Error executing %s"
msgstr ""

#: ../src/mameio.c:691
msgid "receiving data, please wait"
msgstr ""

#: ../src/mameio.c:707
msgid "creating game list, Please wait:"
msgstr ""

#: ../src/mameio.c:962
msgid "xmame not found"
msgstr ""

#: ../src/mameio.c:977
msgid "I don't know how to generate a gamelist for this version of xmame!"
msgstr ""

#: ../src/network_game.c:275
msgid "Network game options"
msgstr ""

#. network mode
#: ../src/network_game.c:290
msgid "Network Mode"
msgstr ""

#. client
#: ../src/network_game.c:301
msgid "Client"
msgstr ""

#: ../src/network_game.c:314
msgid "Server"
msgstr ""

#. Server host:port
#: ../src/network_game.c:326
msgid "Host[:Port] : "
msgstr ""

#. Number of players
#: ../src/network_game.c:346
msgid "Number of players : "
msgstr ""

#. netmap key
#: ../src/network_game.c:367
msgid "Use NetMapKey"
msgstr ""

#. statedebug
#: ../src/network_game.c:377
msgid "Statedebug"
msgstr ""

#. bind
#: ../src/network_game.c:387
msgid "Bind port:"
msgstr ""

#. parallelsync
#: ../src/network_game.c:408
msgid "Perform network input sync in advance"
msgstr ""

#: ../src/options.c:264
msgid "Europe, 1 Slot (also been seen on a 4 slot)"
msgstr ""

#: ../src/options.c:265
msgid "Europe, 4 Slot"
msgstr ""

#: ../src/options.c:266
msgid "US, 2 Slot"
msgstr ""

#: ../src/options.c:267
msgid "US, 6 Slot (V5?)"
msgstr ""

#: ../src/options.c:268
msgid "Asia S3 Ver 6"
msgstr ""

#: ../src/options.c:269
msgid "Japan, Ver 6 VS Bios"
msgstr ""

#: ../src/options.c:270
msgid "Japan, Older"
msgstr ""

#: ../src/options.c:271
msgid "Universe Bios v1.0 (hack)"
msgstr ""

#: ../src/options.c:272
msgid "Universe Bios v1.1 (hack)"
msgstr ""

#: ../src/options.c:273
msgid "Debug (Development) Bios"
msgstr ""

#: ../src/options.c:274
msgid "AES Console (Asia?) Bios"
msgstr ""

#: ../src/options.c:275
msgid "Universe Bios v1.2 (hack)"
msgstr ""

#: ../src/options.c:778 ../data/properties.glade.h:75
msgid "Display"
msgstr ""

#: ../src/options.c:869
msgid "Draw every frame"
msgstr ""

#: ../src/options.c:871
#, c-format
msgid "Skip %i of 12 frames"
msgstr ""

#: ../src/options.c:919 ../src/options.c:2435
msgid "Auto"
msgstr ""

#: ../src/options.c:920
msgid "8 bits"
msgstr ""

#: ../src/options.c:921
msgid "15 bits"
msgstr ""

#: ../src/options.c:922
msgid "16 bits"
msgstr ""

#: ../src/options.c:923
msgid "32 bits"
msgstr ""

#: ../src/options.c:946
msgid "None"
msgstr ""

#: ../src/options.c:947
msgid "Clockwise"
msgstr ""

#: ../src/options.c:948
msgid "Anti-clockwise"
msgstr ""

#: ../src/options.c:1438
msgid "GGI options"
msgstr ""

#: ../src/options.c:1447 ../src/options.c:1542
msgid "linear framebuffer (fast)"
msgstr ""

#: ../src/options.c:1451
msgid "Force resolutions"
msgstr ""

#: ../src/options.c:1529
msgid "SVGA options"
msgstr ""

#: ../src/options.c:1538
msgid "planar (modeX) modes"
msgstr ""

#: ../src/options.c:1546
msgid "Use tweaked VGA modes"
msgstr ""

#: ../src/options.c:1554
msgid "Center X:"
msgstr ""

#: ../src/options.c:1564
msgid "Center Y:"
msgstr ""

#: ../src/options.c:1705 ../data/properties.glade.h:126
msgid "Rendering"
msgstr ""

#. workaround to allow correct translation of "" with gettext
#: ../src/options.c:1842
#, c-format
msgid "Buffer size %s"
msgstr ""

#: ../src/options.c:2017
msgid "Controllers"
msgstr ""

#: ../src/options.c:2115
msgid "Default Layout"
msgstr ""

#: ../src/options.c:2418 ../data/properties.glade.h:115
msgid "Miscellaneous"
msgstr ""

#: ../src/options.c:2436
msgid "Standard"
msgstr ""

#: ../src/options.c:2437
msgid "High"
msgstr ""

#: ../src/progression_window.c:76
msgid "Progress Window"
msgstr ""

#: ../src/progression_window.c:89
msgid "Loading Game..."
msgstr ""

#: ../src/progression_window.c:101
msgid "rom"
msgstr ""

#: ../src/properties.c:80
msgid "Can't audit game"
msgstr ""

#: ../src/properties.c:85 ../src/properties.c:182
msgid "None required"
msgstr ""

#: ../src/properties.c:116 ../src/properties.c:157
msgid "Passed"
msgstr ""

#: ../src/properties.c:118
msgid "Best available"
msgstr ""

#: ../src/properties.c:120
msgid "Not available"
msgstr ""

#: ../src/properties.c:164
msgid "Not found"
msgstr ""

#: ../src/properties.c:312
msgid " (sound)"
msgstr ""

#: ../src/properties.c:409
#, c-format
msgid "%i colors"
msgstr "%i colours"

#: ../src/properties.c:438 ../src/properties.c:441
msgid "Checking..."
msgstr ""

#: ../src/properties.c:505
#, c-format
msgid "Properties for %s"
msgstr ""

#: ../src/properties.c:507
msgid "Default Properties"
msgstr ""

#: ../src/properties.c:522
msgid "Reset to Defaults"
msgstr ""

#: ../src/properties.c:547
msgid "Global game options"
msgstr ""

#: ../src/properties.c:547
msgid "Default options used by all games"
msgstr ""

#: ../src/xmame_executable.c:135
#, c-format
msgid "%s is not a valid xmame executable"
msgstr ""

#: ../src/xmame_executable.c:206
#, c-format
msgid "This version of GMAMEUI supports up to %i xmame executables."
msgstr ""

#: ../data/audit_window.glade.h:1
msgid "0"
msgstr ""

#: ../data/audit_window.glade.h:2
msgid "<b>Checking Game</b>"
msgstr ""

#: ../data/audit_window.glade.h:3
msgid "<b>Details</b>"
msgstr ""

#: ../data/audit_window.glade.h:4
msgid "<b>ROMs</b>"
msgstr ""

#: ../data/audit_window.glade.h:5
msgid "<b>Samples</b>"
msgstr ""

#: ../data/audit_window.glade.h:6
msgid "Best available:"
msgstr ""

#: ../data/audit_window.glade.h:7
msgid "Checking Games"
msgstr ""

#: ../data/audit_window.glade.h:8
msgid "Correct:"
msgstr ""

#: ../data/audit_window.glade.h:9
msgid "Incorrect:"
msgstr ""

#: ../data/audit_window.glade.h:10
msgid "Not found:"
msgstr ""

#: ../data/audit_window.glade.h:11
msgid "Total:"
msgstr ""

#: ../data/audit_window.glade.h:12 ../data/gmameui_prefs.glade.h:22
#: ../data/rom_info.glade.h:15
msgid "gtk-close"
msgstr ""

#: ../data/audit_window.glade.h:13
msgid "gtk-stop"
msgstr ""

#: ../data/audit_window.glade.h:14 ../data/gmameui_prefs.glade.h:23
#: ../data/rom_info.glade.h:16
msgid "label"
msgstr ""

#: ../data/directories.glade.h:1
msgid " "
msgstr ""

#: ../data/directories.glade.h:2
msgid "<b>MAME Executables</b>"
msgstr ""

#: ../data/directories.glade.h:3
msgid "<b>MAME Paths</b>"
msgstr ""

#: ../data/directories.glade.h:4
msgid "<b>MAME Support Files</b>"
msgstr ""

#: ../data/directories.glade.h:5
msgid "<b>Paths to user resources:</b>"
msgstr ""

#: ../data/directories.glade.h:6
msgid "Artwork:"
msgstr ""

#: ../data/directories.glade.h:7
msgid "Browse..."
msgstr ""

#: ../data/directories.glade.h:8
msgid "Cabinets:"
msgstr ""

#: ../data/directories.glade.h:9
msgid "Catver path:"
msgstr ""

#: ../data/directories.glade.h:10
msgid "Cheat file:"
msgstr ""

#: ../data/directories.glade.h:11
msgid "Control panels:"
msgstr ""

#: ../data/directories.glade.h:12
msgid "Controller directory:"
msgstr ""

#: ../data/directories.glade.h:13
msgid "Directories Selection"
msgstr ""

#: ../data/directories.glade.h:14
msgid "Flyers:"
msgstr ""

#: ../data/directories.glade.h:15
msgid "High score file:"
msgstr ""

#: ../data/directories.glade.h:16
msgid "History file:"
msgstr ""

#: ../data/directories.glade.h:17
msgid "Icons path:"
msgstr ""

#: ../data/directories.glade.h:18
msgid "MAME Main Paths"
msgstr ""

#: ../data/directories.glade.h:19
msgid "MAME Paths"
msgstr ""

#: ../data/directories.glade.h:20
msgid "MAME Support Files"
msgstr ""

#: ../data/directories.glade.h:21
msgid "MAME executables"
msgstr ""

#: ../data/directories.glade.h:22
msgid "MAME info file:"
msgstr ""

#: ../data/directories.glade.h:23
msgid "Marquees:"
msgstr ""

#: ../data/directories.glade.h:26
msgid "Snapshots:"
msgstr ""

#: ../data/directories.glade.h:27
msgid "Title screenshots:"
msgstr ""

#: ../data/directories.glade.h:28
msgid "User resources"
msgstr ""

#: ../data/directories.glade.h:29
msgid "User resources:"
msgstr ""

#: ../data/directories.glade.h:30
msgid "gtk-add"
msgstr ""

#: ../data/directories.glade.h:31
msgid "gtk-cancel"
msgstr ""

#: ../data/directories.glade.h:32
msgid "gtk-ok"
msgstr ""

#: ../data/directories.glade.h:33
msgid "gtk-remove"
msgstr ""

#: ../data/gmameui_prefs.glade.h:1
msgid "'The' as a prefix"
msgstr ""

#: ../data/gmameui_prefs.glade.h:2 ../data/options.glade.h:5
msgid "<b>Miscellaneous Options</b>"
msgstr ""

#: ../data/gmameui_prefs.glade.h:3
msgid "<b>Startup Options</b>"
msgstr ""

#: ../data/gmameui_prefs.glade.h:4
msgid "<b>Visible Columns</b>"
msgstr ""

#: ../data/gmameui_prefs.glade.h:5
msgid "Allow game selection with a joystick"
msgstr ""

#: ../data/gmameui_prefs.glade.h:7
msgid "Clone Colour"
msgstr ""

#: ../data/gmameui_prefs.glade.h:11
msgid "Enable version mismatch warning"
msgstr ""

#: ../data/gmameui_prefs.glade.h:12
msgid "GMAMEUI Preferences"
msgstr ""

#: ../data/gmameui_prefs.glade.h:13
msgid "Game Name"
msgstr ""

#: ../data/gmameui_prefs.glade.h:14
msgid "Joystick device:"
msgstr ""

#: ../data/gmameui_prefs.glade.h:16
msgid "Playcount"
msgstr ""

#: ../data/gmameui_prefs.glade.h:17
msgid "Search for new games"
msgstr ""

#: ../data/gmameui_prefs.glade.h:18
msgid "Use MAME default options"
msgstr ""

#: ../data/gmameui_prefs.glade.h:21
msgid "clone_label"
msgstr ""

#: ../data/gmameui_prefs.glade.h:24
msgid "original_label"
msgstr ""

#: ../data/options.glade.h:1
msgid "1"
msgstr ""

#: ../data/options.glade.h:2 ../data/properties.glade.h:15
msgid "<b>Artwork</b>"
msgstr ""

#: ../data/options.glade.h:3
msgid "<b>Debugging</b>"
msgstr ""

#: ../data/options.glade.h:4
msgid "<b>Input</b>"
msgstr ""

#: ../data/options.glade.h:6
msgid "<b>OpenGL</b>"
msgstr ""

#: ../data/options.glade.h:7
msgid "<b>Performance</b>"
msgstr ""

#: ../data/options.glade.h:8 ../data/properties.glade.h:38
msgid "<b>Rotation</b>"
msgstr ""

#: ../data/options.glade.h:9
msgid "<b>Screen</b>"
msgstr ""

#: ../data/options.glade.h:10
msgid "<b>Sound</b>"
msgstr ""

#: ../data/options.glade.h:11
msgid "<b>Vector</b>"
msgstr ""

#: ../data/options.glade.h:12
msgid "<b>Video</b>"
msgstr ""

#: ../data/options.glade.h:13
msgid "Allow uneven stretching"
msgstr ""

#: ../data/options.glade.h:14
msgid "Antialias"
msgstr ""

#: ../data/options.glade.h:15
msgid "Artwork"
msgstr ""

#: ../data/options.glade.h:16
msgid "Audio latency"
msgstr ""

#: ../data/options.glade.h:17
msgid "Automatic Rotation"
msgstr ""

#: ../data/options.glade.h:18
msgid "Automatic frameskip"
msgstr ""

#: ../data/options.glade.h:19
msgid "BIOS name"
msgstr ""

#: ../data/options.glade.h:20
msgid "Beam width:"
msgstr ""

#: ../data/options.glade.h:21
msgid "Brightness"
msgstr ""

#: ../data/options.glade.h:22
msgid "Centre:"
msgstr ""

#: ../data/options.glade.h:23
msgid "Contrast"
msgstr ""

#: ../data/options.glade.h:24
msgid "Controller directory"
msgstr ""

#: ../data/options.glade.h:25
msgid "Convert lightgun button 2 to offscreen support"
msgstr ""

#: ../data/options.glade.h:26 ../data/properties.glade.h:69
msgid "Crop artwork"
msgstr ""

#: ../data/options.glade.h:27
msgid "Debug"
msgstr ""

#: ../data/options.glade.h:28
msgid "Debug script:"
msgstr ""

#: ../data/options.glade.h:29
msgid "Effect name:"
msgstr ""

#: ../data/options.glade.h:30
msgid "Enable cheat mode"
msgstr ""

#: ../data/options.glade.h:31
msgid "Enable input from multiple connected devices"
msgstr ""

#: ../data/options.glade.h:32
msgid "Enable joystick"
msgstr ""

#: ../data/options.glade.h:33
msgid "Enable lightgun"
msgstr ""

#: ../data/options.glade.h:34
msgid "Enable logging"
msgstr ""

#: ../data/options.glade.h:35
msgid "Enable mouse"
msgstr ""

#: ../data/options.glade.h:36
msgid "Enable multithreading"
msgstr ""

#: ../data/options.glade.h:37
msgid "Enable samples"
msgstr ""

#: ../data/options.glade.h:38 ../data/properties.glade.h:86
msgid "Enable sound"
msgstr ""

#: ../data/options.glade.h:39
msgid "Enable steadykey support"
msgstr ""

#: ../data/options.glade.h:40
msgid "Filter"
msgstr ""

#: ../data/options.glade.h:41
msgid "Flicker:"
msgstr ""

#: ../data/options.glade.h:42
msgid "Flip"
msgstr ""

#: ../data/options.glade.h:43 ../data/properties.glade.h:93
msgid "Frames to skip:"
msgstr ""

#: ../data/options.glade.h:44
msgid "Gamma"
msgstr ""

#: ../data/options.glade.h:45
msgid "Horizontal"
msgstr ""

#: ../data/options.glade.h:46 ../data/properties.glade.h:109
msgid "Keep aspect ratio"
msgstr ""

#: ../data/options.glade.h:47
msgid "Keyboard"
msgstr ""

#: ../data/options.glade.h:48
msgid "Left-right"
msgstr ""

#: ../data/options.glade.h:49
msgid "Mouse"
msgstr ""

#: ../data/options.glade.h:50
msgid ""
"No filter\n"
"Bilinear\n"
"Gaussian blur"
msgstr ""

#: ../data/options.glade.h:53
msgid "Number of screens:"
msgstr ""

#: ../data/options.glade.h:54
msgid "Other options"
msgstr ""

#: ../data/options.glade.h:55
msgid "Pause Brightness"
msgstr ""

#: ../data/options.glade.h:56
msgid "Prescale"
msgstr ""

#: ../data/options.glade.h:57
msgid "Refresh speed"
msgstr ""

#: ../data/options.glade.h:58
msgid "Rotate left"
msgstr ""

#: ../data/options.glade.h:59
msgid "Rotate right"
msgstr ""

#: ../data/options.glade.h:60
msgid "Rotation"
msgstr ""

#: ../data/options.glade.h:61
msgid "Run in window"
msgstr ""

#: ../data/options.glade.h:62
msgid "Sample rate"
msgstr ""

#: ../data/options.glade.h:63
msgid "Screen"
msgstr ""

#: ../data/options.glade.h:64
msgid "Show FPS"
msgstr ""

#: ../data/options.glade.h:65
msgid "Skip gameinfo"
msgstr ""

#: ../data/options.glade.h:66
msgid "Sleep"
msgstr ""

#: ../data/options.glade.h:67
msgid ""
"Software\n"
"OpenGL"
msgstr ""

#: ../data/options.glade.h:69
msgid "Speed:"
msgstr ""

#: ../data/options.glade.h:70 ../data/properties.glade.h:139
msgid "Throttle"
msgstr ""

#: ../data/options.glade.h:71
msgid "Up-down"
msgstr ""

#: ../data/options.glade.h:72
msgid "Update in pause"
msgstr ""

#: ../data/options.glade.h:73
msgid "Use backdrops"
msgstr ""

#: ../data/options.glade.h:74
msgid "Use bezels"
msgstr ""

#: ../data/options.glade.h:75
msgid "Use overlays"
msgstr ""

#: ../data/options.glade.h:77
msgid "Verbose logging"
msgstr ""

#: ../data/options.glade.h:78
msgid "Vertical"
msgstr ""

#: ../data/options.glade.h:79
msgid "Video"
msgstr ""

#: ../data/options.glade.h:80 ../data/properties.glade.h:155
msgid "Video mode:"
msgstr ""

#: ../data/options.glade.h:81
msgid "Volume"
msgstr ""

#: ../data/options.glade.h:82
msgid "YUV mode:"
msgstr ""

#: ../data/options.glade.h:83
msgid ""
"aperture1x2rb\n"
"aperture1x3rb\n"
"aperture2x4bg\n"
"aperture2x4rb\n"
"aperture4x6\n"
"scanlines"
msgstr ""

#: ../data/options.glade.h:89 ../data/properties.glade.h:163
msgid "dB"
msgstr ""

#: ../data/options.glade.h:90
msgid "gl_forcepow2texture"
msgstr ""

#: ../data/options.glade.h:91
msgid "gl_glsl"
msgstr ""

#: ../data/options.glade.h:92
msgid "gl_glsl_filter"
msgstr ""

#: ../data/options.glade.h:93
msgid "gl_glsl_vid_attr"
msgstr ""

#: ../data/options.glade.h:94
msgid "gl_notexturerect"
msgstr ""

#: ../data/options.glade.h:95
msgid "gl_pbo"
msgstr ""

#: ../data/options.glade.h:96
msgid "gl_vbo"
msgstr ""

#: ../data/options.glade.h:97
msgid ""
"none\n"
"yv12\n"
"yuy2\n"
"yv12x2\n"
"yuy2x2"
msgstr ""

#: ../data/properties.glade.h:1
msgid ""
"/dev/dsp\n"
"/dev/dsp0\n"
"/dev/dsp1\n"
"/dev/dsp2\n"
"/dev/dsp3\n"
"/dev/audio\n"
"/dev/null"
msgstr ""

#: ../data/properties.glade.h:8
msgid "/dev/mixer"
msgstr ""

#: ../data/properties.glade.h:9
msgid "1:"
msgstr ""

#: ../data/properties.glade.h:10
msgid "2:"
msgstr ""

#: ../data/properties.glade.h:11
msgid "3:"
msgstr ""

#: ../data/properties.glade.h:12
msgid "4:"
msgstr ""

#: ../data/properties.glade.h:13
msgid "<b>Advanced options</b>"
msgstr ""

#: ../data/properties.glade.h:14
msgid "<b>Alsa sound system options</b>"
msgstr ""

#: ../data/properties.glade.h:16
msgid "<b>Buffer size</b>"
msgstr ""

#: ../data/properties.glade.h:17
msgid "<b>CPU:</b>"
msgstr ""

#: ../data/properties.glade.h:18
msgid "<b>Clone of:</b>"
msgstr ""

#: ../data/properties.glade.h:19
msgid "<b>Colors:</b>"
msgstr "<b>Colours:</b>"

#: ../data/properties.glade.h:20
msgid "<b>Control Panel</b>"
msgstr ""

#: ../data/properties.glade.h:21
msgid "<b>Corrections</b>"
msgstr ""

#: ../data/properties.glade.h:22
msgid "<b>Display options</b>"
msgstr ""

#: ../data/properties.glade.h:23
msgid "<b>Effects</b>"
msgstr ""

#: ../data/properties.glade.h:24
msgid "<b>FX (Glide) options</b>"
msgstr ""

#: ../data/properties.glade.h:25
msgid "<b>Frame skipping</b>"
msgstr ""

#: ../data/properties.glade.h:26
msgid "<b>General sound options</b>"
msgstr ""

#: ../data/properties.glade.h:27
msgid "<b>Joystick</b>"
msgstr ""

#: ../data/properties.glade.h:28
msgid "<b>Key mapping</b>"
msgstr ""

#: ../data/properties.glade.h:29
msgid "<b>Keyboard</b>"
msgstr ""

#: ../data/properties.glade.h:30
msgid "<b>Manufacturer:</b>"
msgstr ""

#: ../data/properties.glade.h:31
msgid "<b>Mouse / Trackball</b>"
msgstr ""

#: ../data/properties.glade.h:32
msgid "<b>OpenGL options</b>"
msgstr ""

#: ../data/properties.glade.h:33
msgid "<b>Other options</b>"
msgstr ""

#: ../data/properties.glade.h:34
msgid "<b>Photon options</b>"
msgstr ""

#: ../data/properties.glade.h:35
msgid "<b>QNX options</b>"
msgstr ""

#: ../data/properties.glade.h:36
msgid "<b>Resolution</b>"
msgstr ""

#: ../data/properties.glade.h:37
msgid "<b>Rom check:</b>"
msgstr ""

#: ../data/properties.glade.h:39
msgid "<b>SDL options</b>"
msgstr ""

#: ../data/properties.glade.h:40
msgid "<b>Sample Check:</b>"
msgstr ""

#: ../data/properties.glade.h:41
msgid "<b>Screen:</b>"
msgstr ""

#: ../data/properties.glade.h:42
msgid "<b>Sound:</b>"
msgstr ""

#: ../data/properties.glade.h:43
msgid "<b>Vector options</b>"
msgstr ""

#: ../data/properties.glade.h:44
msgid "<b>Video mode</b>"
msgstr ""

#: ../data/properties.glade.h:45
msgid "<b>Volume attenuation</b>"
msgstr ""

#: ../data/properties.glade.h:46
msgid "<b>X11 options</b>"
msgstr ""

#: ../data/properties.glade.h:47
msgid "<b>Xinput joystick</b>"
msgstr ""

#: ../data/properties.glade.h:48
msgid "<b>Xinput trackball</b>"
msgstr ""

#: ../data/properties.glade.h:49
msgid "<b>Year:</b>"
msgstr ""

#: ../data/properties.glade.h:50
msgid "<b>aRts options</b>"
msgstr ""

#: ../data/properties.glade.h:51
msgid "Alphablending"
msgstr ""

#: ../data/properties.glade.h:52
msgid "Analog joystick"
msgstr ""

#: ../data/properties.glade.h:53
msgid "Antialiasing"
msgstr ""

#: ../data/properties.glade.h:54
msgid "Artwork resolution:"
msgstr ""

#: ../data/properties.glade.h:55
msgid "Audio device:"
msgstr ""

#: ../data/properties.glade.h:56
msgid "Auto Resolution"
msgstr ""

#: ../data/properties.glade.h:57
msgid "Autodouble"
msgstr ""

#: ../data/properties.glade.h:58
msgid "Automatic"
msgstr ""

#: ../data/properties.glade.h:59
msgid "Backdrops"
msgstr ""

#: ../data/properties.glade.h:60
msgid "Beam size"
msgstr ""

#: ../data/properties.glade.h:61
msgid "Bezels"
msgstr ""

#: ../data/properties.glade.h:62
msgid "Bilinear filtering"
msgstr ""

#: ../data/properties.glade.h:63
msgid "Bit per pixels:"
msgstr ""

#: ../data/properties.glade.h:64
msgid "Brightness correction"
msgstr ""

#: ../data/properties.glade.h:65
msgid "Cabinet model:"
msgstr ""

#: ../data/properties.glade.h:66
msgid "Card ID:"
msgstr ""

#: ../data/properties.glade.h:67
msgid "Color modulation"
msgstr "Colour modulation"

#: ../data/properties.glade.h:68
msgid "Controller:"
msgstr ""

#: ../data/properties.glade.h:70
msgid "DSP Plugin:"
msgstr ""

#: ../data/properties.glade.h:71
msgid "Debug windows (for developers)"
msgstr ""

#: ../data/properties.glade.h:73
msgid "Device #:"
msgstr ""

#: ../data/properties.glade.h:74
msgid "Disable mode:"
msgstr ""

#: ../data/properties.glade.h:76
msgid "Do not apply rotation"
msgstr ""

#: ../data/properties.glade.h:77
msgid "Double Buffering"
msgstr ""

#: ../data/properties.glade.h:78
msgid "Double buffering"
msgstr ""

#: ../data/properties.glade.h:79
msgid "Draw antialiased vectors"
msgstr ""

#: ../data/properties.glade.h:80
msgid "Draw bitmap"
msgstr ""

#: ../data/properties.glade.h:81
msgid "Draw only changes"
msgstr ""

#: ../data/properties.glade.h:82
msgid "Draw translucent vectors"
msgstr ""

#: ../data/properties.glade.h:83
msgid "Effect to use:"
msgstr ""

#: ../data/properties.glade.h:84
msgid "Enable Windows keys"
msgstr ""

#: ../data/properties.glade.h:85
msgid "Enable game cheats"
msgstr ""

#: ../data/properties.glade.h:87
msgid "Fake sound"
msgstr ""

#: ../data/properties.glade.h:88
msgid "File:"
msgstr ""

#: ../data/properties.glade.h:89
msgid "Flicker"
msgstr ""

#: ../data/properties.glade.h:90
msgid "Flip screen left-right"
msgstr ""

#: ../data/properties.glade.h:91
msgid "Flip screen upside-down"
msgstr ""

#: ../data/properties.glade.h:92
msgid "Force YUV mode:"
msgstr ""

#: ../data/properties.glade.h:94
msgid "Full Screen"
msgstr ""

#: ../data/properties.glade.h:95
msgid "Fullscreen"
msgstr ""

#: ../data/properties.glade.h:96
msgid "GL extension #78"
msgstr ""

#: ../data/properties.glade.h:97
msgid "GLU library:"
msgstr ""

#: ../data/properties.glade.h:98
msgid "Gamma correction"
msgstr ""

#: ../data/properties.glade.h:99
msgid "General"
msgstr ""

#: ../data/properties.glade.h:100
msgid "Geometry:"
msgstr ""

#: ../data/properties.glade.h:101
msgid "Grab keyboard"
msgstr ""

#: ../data/properties.glade.h:102
msgid "Grab mouse"
msgstr ""

#: ../data/properties.glade.h:103
msgid "Height scale:"
msgstr ""

#: ../data/properties.glade.h:104
msgid "HotRod SE support"
msgstr ""

#: ../data/properties.glade.h:105
msgid "HotRod support"
msgstr ""

#: ../data/properties.glade.h:106
msgid "Intensity"
msgstr ""

#: ../data/properties.glade.h:107
msgid "Joystick device prefix:"
msgstr ""

#: ../data/properties.glade.h:108
msgid "Joystick type:"
msgstr ""

#: ../data/properties.glade.h:110
msgid "Keep aspect ratio:"
msgstr ""

#: ../data/properties.glade.h:111
msgid "Keyboard Layout type:"
msgstr ""

#: ../data/properties.glade.h:112
msgid "Log debug info"
msgstr ""

#: ../data/properties.glade.h:113
msgid "Max frameskip:"
msgstr ""

#: ../data/properties.glade.h:114
msgid "Method:"
msgstr ""

#: ../data/properties.glade.h:116
msgid "Mixer device:"
msgstr ""

#: ../data/properties.glade.h:117
msgid "Mixer plugin:"
msgstr ""

#: ../data/properties.glade.h:118
msgid ""
"Name of pad device:\n"
"(FM townpad)"
msgstr ""

#: ../data/properties.glade.h:120
msgid "Neo-Geo Bios:"
msgstr ""

#: ../data/properties.glade.h:121
msgid "OpenGL library:"
msgstr ""

#: ../data/properties.glade.h:122
msgid "Overlays"
msgstr ""

#: ../data/properties.glade.h:123
msgid "PCM:"
msgstr ""

#: ../data/properties.glade.h:124
msgid "Private colormap"
msgstr "Private colourmap"

#: ../data/properties.glade.h:125
msgid "Rapid-fire"
msgstr ""

#: ../data/properties.glade.h:127
msgid "Resolution"
msgstr ""

#: ../data/properties.glade.h:128
msgid "Resolution:"
msgstr ""

#: ../data/properties.glade.h:129
msgid "SDL modes:"
msgstr ""

#: ../data/properties.glade.h:130
msgid "Sample rate:"
msgstr ""

#: ../data/properties.glade.h:131
msgid "Scale video to height:"
msgstr ""

#: ../data/properties.glade.h:132
msgid "Show cursor"
msgstr ""

#: ../data/properties.glade.h:133
msgid "Skip disclaimer info"
msgstr ""

#: ../data/properties.glade.h:134
msgid "Skip game info"
msgstr ""

#: ../data/properties.glade.h:135
msgid "Sleep when idle"
msgstr ""

#: ../data/properties.glade.h:137
msgid "Sound file name:"
msgstr ""

#: ../data/properties.glade.h:138
msgid "Texture size:"
msgstr ""

#: ../data/properties.glade.h:140
msgid "Timer based audio"
msgstr ""

#: ../data/properties.glade.h:141
msgid "True color blitter"
msgstr "True colour blitter"

#: ../data/properties.glade.h:142
msgid "UGCI (tm) Coin/Play support"
msgstr ""

#: ../data/properties.glade.h:143
msgid "USB PS Game Pads"
msgstr ""

#: ../data/properties.glade.h:144
msgid "Use MIT shared memory"
msgstr ""

#: ../data/properties.glade.h:145
msgid "Use Scanlines"
msgstr ""

#: ../data/properties.glade.h:146
msgid "Use additional game artwork:"
msgstr ""

#: ../data/properties.glade.h:147
msgid "Use additional options:"
msgstr ""

#: ../data/properties.glade.h:148
msgid "Use config name:"
msgstr ""

#: ../data/properties.glade.h:149
msgid "Use fixed resolution:"
msgstr ""

#: ../data/properties.glade.h:150
msgid "Use keyboard LEDs as game indicators"
msgstr ""

#: ../data/properties.glade.h:151
msgid "Use mouse"
msgstr ""

#: ../data/properties.glade.h:152
msgid "Use preferred device"
msgstr ""

#: ../data/properties.glade.h:153
msgid "Use samples"
msgstr ""

#: ../data/properties.glade.h:156
msgid "Width scale:"
msgstr ""

#: ../data/properties.glade.h:157
msgid "Window size:"
msgstr ""

#: ../data/properties.glade.h:158
msgid "X Synch"
msgstr ""

#: ../data/properties.glade.h:159
msgid "X-based device:"
msgstr ""

#: ../data/properties.glade.h:160
msgid "XIL multi-threading"
msgstr ""

#: ../data/properties.glade.h:161
msgid "XIL scaling"
msgstr ""

#: ../data/properties.glade.h:162
msgid "aRts buffer delay time:"
msgstr ""

#: ../data/properties.glade.h:164
msgid "frames"
msgstr ""

#: ../data/rom_info.glade.h:1
msgid "<b>About this ROM</b>"
msgstr ""

#: ../data/rom_info.glade.h:2
msgid "<b>Audit Details</b>"
msgstr ""

#: ../data/rom_info.glade.h:3
msgid "<b>Screen Details</b>"
msgstr ""

#: ../data/rom_info.glade.h:4
msgid "<b>Technical Details</b>"
msgstr ""

#: ../data/rom_info.glade.h:5
msgid "CPU:"
msgstr ""

#: ../data/rom_info.glade.h:6
msgid "Clone of:"
msgstr ""

#: ../data/rom_info.glade.h:7
msgid "Colors:"
msgstr "Colours:"

#: ../data/rom_info.glade.h:9
msgid "Manufacturer:"
msgstr ""

#: ../data/rom_info.glade.h:10
msgid "Rom check:"
msgstr ""

#: ../data/rom_info.glade.h:11
msgid "Sample Check:"
msgstr ""

#: ../data/rom_info.glade.h:12
msgid "Screen:"
msgstr ""

#: ../data/rom_info.glade.h:13
msgid "Sound:"
msgstr ""

#: ../data/rom_info.glade.h:14
msgid "Year:"
msgstr ""

#: ../data/sidebar.glade.h:1
msgid "Cabinet"
msgstr ""

#: ../data/sidebar.glade.h:2
msgid "Control Panel"
msgstr ""

#: ../data/sidebar.glade.h:3
msgid "Flyer"
msgstr ""

#: ../data/sidebar.glade.h:4
msgid "Marquee"
msgstr ""

#: ../data/sidebar.glade.h:5
msgid "Snapshot"
msgstr ""

#: ../data/sidebar.glade.h:6
msgid "Title Snapshot"
msgstr ""

#: ../gmameui.desktop.in.h:1
msgid "Arcade Machine Emulator Frontend"
msgstr ""

#: ../gmameui.desktop.in.h:2
msgid "GMAMEUI Arcade Machine Emulator Frontend"
msgstr ""

#: ../gmameui.desktop.in.h:3
msgid "Play and browse classic arcade games"
msgstr ""
