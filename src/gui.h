/*
 * GMAMEUI
 *
 * Copyright 2007-2009 Andrew Burton <adb@iinet.net.au>
 * based on GXMame code
 * 2002-2005 Stephane Pontier <shadow_walker@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef __GUI_H__
#define __GUI_H__

#include "common.h"

#include <gtk/gtkcheckmenuitem.h>
#include <gtk/gtkframe.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkpaned.h>
#include <gtk/gtkprogressbar.h>
#include <gtk/gtkstatusbar.h>
#include <gtk/gtktextbuffer.h>
#include <gtk/gtktogglebutton.h>
#include <gtk/gtktoolbar.h>

#include "gmameui.h"
#include "unzip.h"
#include "callbacks.h"
#include "mame_options.h"
#include "mame_options_legacy.h"
#include "filters_list.h"
#include "gui_prefs.h"
#include "interface.h"
#include "gmameui-sidebar.h"
#include "gmameui-gamelist-view.h"
#include "mame-exec-list.h"
#include "gmameui-gamelist-view.h"

/* The following menu entries are always enabled */
static const GtkActionEntry gmameui_always_sensitive_menu_entries[] =
{
	/* Toplevel */
	{ "File", NULL, N_("_File") },
	{ "View", NULL, N_("_View") },
	{ "Options", NULL, N_("_Options") },
	{ "Help", NULL, N_("_Help") },

	/* File menu */
	{ "FileSelectRandom", NULL, N_("_Select Random Game"), NULL,
	  N_("Play currently selected game"), G_CALLBACK (on_select_random_game_activate) },
	{ "FileQuit", GTK_STOCK_QUIT, N_("_Quit"), "<control>Q",
	  N_("Quit GMAMEUI"), G_CALLBACK (on_exit_activate) },
	
	/* View menu */
	{ "ViewRefresh", GTK_STOCK_REFRESH, N_("Refresh"), "F5",
	  N_("Refresh game list"), G_CALLBACK (on_refresh_activate) },

	/* Option menu */
	{ "OptionDirs", NULL, N_("_Directories..."), NULL,
	  N_("Set directory configuration"), G_CALLBACK (on_directories_menu_activate) },  
	{ "OptionPreferences", GTK_STOCK_PREFERENCES, N_("_GMAMEUI Preferences..."), NULL,
	  N_("Set GMAMEUI preferences"), G_CALLBACK (on_preferences_activate) },  
	  	  
	/* Help menu */
	{"HelpContents", GTK_STOCK_HELP, N_("_Contents"), "F1",
	 N_("Open the GMAMEUI manual"), G_CALLBACK (on_help_activate) },
	{ "HelpAbout", GTK_STOCK_ABOUT, NULL, NULL,
	 N_("About this application"), G_CALLBACK (on_about_activate) }
};

/* The following menu entries are enabled when a ROM is selected and a MAME
   executable exists */
static const GtkActionEntry gmameui_rom_and_exec_menu_entries[] =
{
	/* File menu */
	{ "FilePlayGame", NULL, N_("Play"), NULL,
	  N_("Play currently selected game"), G_CALLBACK (on_play_activate) },
	{ "FilePlayRecord", GTK_STOCK_SAVE, N_("Play and Record Input..."), NULL,
	  N_("Record a game for later playback"), G_CALLBACK (on_play_and_record_input_activate) },
	{ "FilePlaybackRecord", GTK_STOCK_OPEN, N_("Playback Input..."), NULL,
	  N_("Playback a recorded game"), G_CALLBACK (on_playback_input_activate) },
	{ "FileOptions", GTK_STOCK_PROPERTIES, N_("Options"), NULL,
	  N_("Change the options of the selected game"), G_CALLBACK (on_options_activate) }, 
};

/* The following menu entries are enabled when MAME executable exists */
static const GtkActionEntry gmameui_exec_menu_entries[] =
{
	{ "FileAuditAllGames", NULL, N_("_Audit All Games"), NULL,
	  N_("Audit ROM and sample sets"), G_CALLBACK (on_audit_all_games_activate) },
	{ "OptionRebuildGameList", NULL, N_("_Rebuild Game List"), NULL,
	  N_("Rebuild the game list from executable information"), G_CALLBACK (on_rebuild_game_list_menu_activate) },
	{ "OptionDefaultOpts", NULL, N_("Default _Options..."), NULL,
	  N_("Set default game options"), G_CALLBACK (on_options_default_activate) },
};

/* The following menu entries are enabled when a ROM is selected */
static const GtkActionEntry gmameui_rom_menu_entries[] =
{
	{ "FileProperties", GTK_STOCK_INFO, N_("Properties"), NULL,
	  N_("Display the properties of the selected game"), G_CALLBACK (on_properties_activate) }, 
};

static const GtkActionEntry gmameui_favourite_menu_entries[] =
{
	{ "FileFavesAdd", GTK_STOCK_ADD, N_("Add to 'Favorites'"), NULL,
	  N_("Add this game to your 'Favorites' game folder"), G_CALLBACK (on_add_to_favorites_activate) },
	{ "FileFavesRemove", GTK_STOCK_REMOVE, N_("Remove from 'Favorites'"), NULL,
	  N_("Remove this game from your 'Favorites' game folder"), G_CALLBACK (on_remove_from_favorites_activate) },
};

#ifdef TREESTORE
/* The following menu entries are enabled when the view is changed to a tree view */
static const GtkActionEntry gmameui_view_expand_menu_entries[] =
{
	{ "ViewExpandAll", NULL, N_("Expand All"), NULL,
	  N_("Expand all rows"), G_CALLBACK (on_expand_all_activate) },
	{ "ViewCollapseAll", NULL, N_("Collapse All"), NULL,
	  N_("Collapse all rows"), G_CALLBACK (on_collapse_all_activate) },
};
#endif
static const GtkToggleActionEntry gmameui_view_toggle_menu_entries[] = 
{
	{ "ViewToolbar", NULL, N_("_Toolbar"), "<alt>T",
	  N_("Show or hide the toolbar"),
	  G_CALLBACK (on_toolbar_view_menu_activate), TRUE },
	{ "ViewStatusBar", NULL, N_("_Status Bar"), "<alt>S",
	  N_("Show or hide the status bar"),
	  G_CALLBACK (on_status_bar_view_menu_activate), TRUE },  
	{ "ViewFolderList", NULL, N_("Fold_er List"), "<alt>D",
	  N_("Show or hide the folder list"),
	  G_CALLBACK (on_folder_list_activate), TRUE },  
	{ "ViewSidebarPanel", NULL, N_("Scree_nshot Panel"), "<alt>N",
	  N_("Show or hide the screenshot panel"),
	  G_CALLBACK (on_screen_shot_activate), TRUE },   
};

static const GtkRadioActionEntry gmameui_view_radio_menu_entries[] =
{
	{ "ViewListView", NULL, N_("_List"), NULL,
	  N_("Displays items in a list"), LIST },
#ifdef TREESTORE
	{ "ViewTreeView", NULL, N_("List _Tree"), NULL,
	  N_("Displays items in a tree list with clones indented"), LIST_TREE },
#endif
	{ "ViewDetailsListView", NULL, N_("_Details"), NULL,
	  N_("Displays detailed information about each item"), DETAILS },
#ifdef TREESTORE
	{ "ViewDetailsTreeView", NULL, N_("Detai_ls Tree"), NULL,
	  N_("Displays detailed information about each item with clones indented"), DETAILS_TREE },
#endif
};

static const GtkActionEntry gmameui_column_entries[] =
{
	{ "ColumnHide", NULL, N_("Hide Column"), NULL,
	  N_("Hide Column"), G_CALLBACK (on_column_hide_activate) },
};



GtkWidget *MainWindow;

struct main_gui_struct {

	GtkToolbar *toolbar;

	GtkWidget *combo_progress_bar;
	GtkStatusbar *status_progress_bar;
	GtkProgressBar *progress_progress_bar;
	GtkWidget *tri_status_bar;
	GtkStatusbar *statusbar1;
	GtkStatusbar *statusbar2;
	GtkStatusbar *statusbar3;

	GtkPaned *hpanedLeft;
	GtkPaned *hpanedRight;

	GtkWidget *scrolled_window_filters;
	GMAMEUIFiltersList *filters_list;

	GtkWidget        *scrolled_window_games;
	MameGamelistView *displayed_list;           /* The GtkTreeView displaying the ROMs */
	GtkWidget        *search_entry;             /* GtkEntry used for searching tree view */

	GMAMEUISidebar *screenshot_hist_frame;

	GtkWidget *executable_menu;
	
	GtkUIManager *manager;
	GtkActionGroup *gmameui_rom_action_group;   /* Item entries that require a ROM */
	GtkActionGroup *gmameui_rom_exec_action_group;  /* Item entries that require both a ROM and an exec */
	GtkActionGroup *gmameui_exec_action_group;  /* Item entries that require an exec */
	GtkActionGroup *gmameui_favourite_action_group;
#ifdef TREESTORE
	GtkActionGroup *gmameui_view_action_group;
#endif
	GtkActionGroup *gmameui_exec_radio_action_group;	/* Executable radio buttons */
	gint gmameui_exec_merge_id;
	
	MameOptions *options;
	MameOptionsLegacy *legacy_options;
	
	MameGuiPrefs *gui_prefs;
	
	MameExecList *exec_list;
};

struct main_gui_struct main_gui;

void adjustment_scrolled (GtkAdjustment *adjustment,
                          gpointer       user_data);

/* New icon code */
void gmameui_icons_init (void);
GdkPixbuf * gmameui_get_icon_from_stock (const char *);
GtkWidget * gmameui_get_image_from_stock (const char *);

void add_exec_menu(void);
void init_gui(void);

GdkPixbuf * get_icon_for_rom (MameRomEntry *rom, guint size, ZIP *zip);
GdkPixbuf * gmameui_get_icon_from_stock (const char *id);
GtkWidget * gmameui_get_image_from_stock (const char *id);
void get_status_icons (void);
void gmameui_icons_init (void);


void gamelist_popupmenu_show (GdkEventButton *event);
void gmameui_ui_set_favourites_sensitive (gboolean rom_is_favourite);
void gmameui_ui_set_items_sensitive (void);
void gmameui_menu_set_view_mode_check (gint view_mode, gboolean state);

void select_inp (gboolean play_record);
void select_game (MameRomEntry *rom);

#endif /* __GUI_H__ */
